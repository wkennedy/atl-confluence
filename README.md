defaults:
main.yml

handlers:
main.yml

tasks:
main.yml

templates:
confluence-config.properties.j2
confluence.service.j2
